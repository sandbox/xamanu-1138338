; Drush make version
api = "2"

; DRUPAL CORE
core = "6.x"
projects[] = drupal


; CONTRIB MODULES

projects[advanced_help][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[features][subdir] = contrib
projects[date][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[devel][subdir] = "contrib"

projects[views][subdir] = "contrib"
projects[views][version] = "3.0-alpha3"

projects[timeline][type] = "module"
projects[timeline][subdir] = "contrib"
projects[timeline][download][type] = "git"
projects[timeline][version] = "master"
projects[timeline][download][url] = "http://git.drupal.org/project/timeline.git"

; SIMILE Timeline Libraries
libraries[timeline_js][download][type] = "svn"
libraries[timeline_js][download][url] = "http://simile-widgets.googlecode.com/svn/timeline/trunk/src/webapp/api"
libraries[timeline_js][directory_name] = "simile_timeline/timeline_js"

libraries[timeline_ajax][download][type] = "svn"
libraries[timeline_ajax][download][url] = "http://simile-widgets.googlecode.com/svn/ajax/trunk/src/webapp/api"
libraries[timeline_ajax][directory_name] = "simile_timeline/timeline_ajax"




